package com.ganesh.learn.android.exploreswipelistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.fortysevendeg.swipelistview.SwipeListView;

import java.util.List;

/**
 * Created by Ganesh on 17-05-2015.
 */
public class CustomAdapter extends BaseAdapter {
    private final Context context;
    private final List<Person> data;

    public CustomAdapter(Context context, List<Person> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Person getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Person person = getItem(position);
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_layout, parent, false);

            holder = new ViewHolder();
            holder.deleteButton = (Button) convertView.findViewById(R.id.deleteButton);
            holder.nameText = (TextView) convertView.findViewById(R.id.name);
            holder.addressText = (TextView) convertView.findViewById(R.id.address);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final SwipeListView swipeListView = (SwipeListView) parent;
        swipeListView.recycle(convertView, position);

        holder.addressText.setText(person.getAddress());
        holder.nameText.setText(person.getName());
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swipeListView.dismiss(position);
            }
        });

        return convertView;
    }

    static class ViewHolder {
        Button deleteButton;
        TextView nameText;
        TextView addressText;
    }
}
