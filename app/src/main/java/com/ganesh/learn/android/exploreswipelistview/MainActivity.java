package com.ganesh.learn.android.exploreswipelistview;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    private static final String TAG = "MainActivity";
    private SwipeListView listView;
    private CustomAdapter adapter;
    private List<Person> personList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (SwipeListView) findViewById(R.id.list);

        setUpListView();
    }

    private void setUpListView() {
        listView.setAdapter(createAdapter());
        listView.setSwipeListViewListener(new SwipeListListener());
    }

    private class SwipeListListener extends BaseSwipeListViewListener {

        @Override
        public void onDismiss(int[] reverseSortedPositions) {
            for (int pos : reverseSortedPositions) {
                personList.remove(pos);
            }
            adapter.notifyDataSetChanged();
        }
    }

    private CustomAdapter createAdapter() {
        personList = new ArrayList<>();
        personList.add(new Person("Ganesh Kumar", "E1503, Green Valley Park"));
        personList.add(new Person("Anita Desai", "A501, Green Valley Park"));
        personList.add(new Person("Latha Fernandis", "B103, Green Valley Park"));
        personList.add(new Person("Bhavesh Sharma", "C203, Green Valley Park"));
        personList.add(new Person("Prateep Singhal", "D1003, Green Valley Park"));
        personList.add(new Person("Srinivas Reddy", "E504, Green Valley Park"));
        personList.add(new Person("Pradeep Singh", "F203, Green Valley Park"));
        personList.add(new Person("Praveen Verma", "F203, Green Valley Park"));
        personList.add(new Person("Rajesh Katadia", "F303, Green Valley Park"));
        personList.add(new Person("Harish Dwivedi", "F403, Green Valley Park"));
        personList.add(new Person("Sriranga Kannan", "F503, Green Valley Park"));
        personList.add(new Person("Prabal Mishra", "F603, Green Valley Park"));
        adapter = new CustomAdapter(this, personList);
        return adapter;
    }
}
